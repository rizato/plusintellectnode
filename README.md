# README #

This project was made so that I didn't have to check [/r/wyvernrpg](https://reddit.com/r/wyvernrpg) all the time for updates on the game Wyvern.

This is the server side of things. You can throw this on a server, or a local machine with nodejs, and it will poll Rhialto's account for new posts. When it sees one it will save it to the mongo database & send out a notification to all registered users. 

### How do I get set up? ###

* Clone the repo
* Install nodejs
* Create an project on the Google API console
* Enable GCM. Record key & sender ID
* Create a developer account on Reddit
* Build the configuration
* run `node start`
* Setup the Android app.

# Config.js

Fill in the details for the file below, and save it in a file called config.js in the main directory.

```javascript
var config = {
    track_sub: 'wyvernrpg',
    track_user: 'rhialto',
	reddit: {
		script: '<script id>',
		key: '<key>',
		username: '<reddit dev bot account username>',
		password: '<reddit password>',
		user_agent: 'nodejs:<project name>:v1.0.0 (by /u/real_reddit_account)'
	},
	gcm: {
		message: "New update from R!",
		key: 'key=<google cloud messaging secret>'
	},
    proto_file: 'rupdates.proto'
};

module.exports = config;
```


# License

```
The MIT License (MIT)
Copyright (c) 2016 Robert Lathrop

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
```