var grpc = require('grpc');
var mongoose = require('mongoose');
var async = require('async');
var request = require('request');
var config = require('./config.js');
var Gcm = require('./model/gcm.js');
var Update = require('./model/updates.js');

var protoDescriptor = grpc.load(__dirname + '/'+config.proto_file);
var updates = protoDescriptor.updates;

//10 minute delay on scans
var delay = 10 * 60 * 1000;

/*
 * A part of the GRPC protocol. This function takes in a token, generated
 * in the application as defined in the GCM documentaion. It adds that token
 * to the database of tokens, then returns a status code. -1 is bad. 0 is good.
 */
function register(call, callback) {
    console.log("register");
    //Add call.request.token to database
    Gcm.findOne({token: call.request.token}, function(err, user) {
        if (err) {
            console.log(err);
            callback(err);
            return;
        }
        if (user) {
            console.log(user.token);
            //Already exists, but needs a good response
            callback(null, {status:0});
            return
        }
        var user = new Gcm({
            token: call.request.token
            });
        user.save(function(err) {
            if (err) {
                callback(err);
                return;
            }
            console.log("Added user");
            callback(null, {status:0});
        });
    });
}

/*
 * A part of the GRPC protocol. This function takes in a token, generated
 * in the application as defined in the GCM documentaion. It removes the 
 * token from the database, then returns a status code. -1 is bad. 0 is good.
 */
function unregister(call, callback) {
    console.log("unregister");
    //Add call.request.token to database
    Gcm.findOneAndRemove({token: call.request.token}, function(err, user) { 
        if (err) {
            callback(null, {status:1});
            return;
        }
        if (user) {
            console.log("Removed user" + user.token);
            callback(null,{status:0});
        } else {
            console.log("User not found");
            callback(null,{status:1});
        }
    });
}

/*
 * A part of the GRPC protocol. This function is a streaming function
 * which takes in an empty message, and returns a series of PostReply messages.
 */
function getPosts(call) {
    console.log("get posts");
    //Ignoring the input into getPosts.
    //Use this as the DB entry. call.request.string
    Update.find({}, function(err, updates) {
        if (err) {
            call.end();
            return;
        }
        //Iterate async & write a message for each.
        if (updates) {
            async.each(updates, function(update, callback) {
                var post = {
                    message: update.title+'',
                    link: update.url+'',
                    date: update.date+''
                }
                //Write the object to the client
                call.write(post);
                callback();
            }, function(err) {
                //Sends the end signal to client
                if (err) {
                    console.log(err);
                    //Intentional not return
                }
                //Tells the client that is all.
                call.end();
                return;
            });
        } else {
            call.end();
        }
    });
}


/* 
 * This function reaches out to reddit via their API. It pulls back all of the
 * posts by the user specified in the config. Then, it checks for any posts
 * with the same link in the DB. If not, adds the post. At the end, it calls
 * callback with the list of new posts. 
 */
function pullFromReddit(callback) {
    var body = 'grant_type=password&username='+config.reddit.username+'&password='+ config.reddit.password;
    var options = {
        url: 'https://www.reddit.com/api/v1/access_token',
        auth: {
            user: config.reddit.script,
            pass: config.reddit.key
        },
        body: body,
        headers: {
            'User-Agent': config.reddit.user_agent 
        }
    }
    console.log("Sending request to reddit");
    request.post(options, function (err, response, body) {
        if (err) {
            callback(err);
            return;
        } 
        //Does this for any good response code. I don't care which.
        console.log("Reddit response: " + response.statusCode);
        if (response.statusCode / 100 == 2) {
            var res = JSON.parse(body);
            var auth = res.token_type+' '+res.access_token;
            var url = 'https://oauth.reddit.com/user/'+config.track_user+'/submitted';
            console.log(url);
            var opt = {
                url: url,
                headers: {
                    'User-Agent': config.reddit.user_agent,
                    'Authorization': auth,
                }
            };
            //Hitting the API again for the actual posts.
            var posts = [];
            request.get(opt, function (e, res, b) {
                console.log(res.statusCode)
                if (e) {
                    callback(e);
                    return;
                }
                var children = JSON.parse(b).data.children;
                async.forEach(children, function(child, call) {
                    //Each post is data.children[i].data. (title|permalink|url|subreddit|created_utc)
                    if (child.data.subreddit == config.track_sub) {
                        console.log("Looking for: " + child.data.permalink);
                        Update.findOne({url: child.data.permalink}, function(err, post) {
                            if (err) {
                                call(err);
                                return;
                            }
                            if(post) {
                                console.log("Found "+ child.data.permalink);
                                call();
                            } else {
                                var post = {
                                    title: child.data.title,
                                    url: child.data.permalink,
                                    //Reddit does dates in seconds.
                                    date: child.data.created_utc+'000'
                                }
                                console.log("saving "+post);
                                posts.push(post);
                                var p = new Update(post);
                                p.save(function(err) {
                                    if (err) {
                                        console.log("Didn't save");
                                    }
                                    call();
                                });
                            }
                        });
                    } else {
                        call();
                    }
                }, function(err) {
                    //this is where this finally finishes
                    if (err) {
                        callback(err);
                    } else {
                        callback(null, posts);
                    }
                });
            });
        }
    });
}

/*
 * This function sends out GCM messages to each and every phone.
 *
 * It should really use device groups, but I don't really feel
 * like dividing up everyone into groups of 20. (As if there will
 * really be more than 20 users, ha.
 */
function alertUsers(post, postCallback) {
    Gcm.find({}, function(err, users) {
        if (err) {
            console.log("Failed to alert users");
            return;
        }
        //Iterates through each and sends a GCM notification    
        console.log("Updating users on a new post");
        if (users) {
            var userTokens = [];
            users.forEach(function(user) {
                userTokens.push(user.token);
            });
            if (userTokens.length > 0 ) {
                var options = {
                    url: 'https://gcm-http.googleapis.com/gcm/send',
                    body: JSON.stringify({
                        "registration_ids": userTokens,
                        "data": {
                            "message": config.gcm.message,
                            "title": post.title,
                            "link": post.url,
                            "date": post.date
                        }
                    }),
                    headers: {
                        'Authorization': config.gcm.key,
                        'Content-Type': 'application/json'
                    }
                };
                request.post(options, function(err, response, body) {
                    console.log(body);
                    if (err) {
                        postCallback(err);
                        return;
                    }
                    postCallback();
                });
            } else {
                postCallback();
            }
        }
    });
}
/*
 * Create a server from the GRPC .proto file with the functions defined above.
 */
function getServer() {
    var server = new grpc.Server();
    server.addProtoService(updates.Updates.service, {
        register: register,
        unregister: unregister,
        getPosts: getPosts
    });
    return server;
}

/*
 * This function holds the main functionality for kicking off this server.
 */
function main() {
    mongoose.connect('mongodb://localhost/wyvernupdates');
    //Setup an event for every 10 minutes. This checks Reddit & sends out 
    //GCM's to registered users
    setInterval(function() {
        pullFromReddit(function(err, posts) {
            if (err) {
                console.log(err);
                return;
            }
            //These are just the new posts
            async.each(posts, function(post, callback) {
                //passes the callback onto alertUsers, since it is also async
                alertUsers(post, callback);
            }, function(err) {
                if (err) {
                    console.log(err);
                }
                console.log('Finished all interval async tasks');
                
            });
        });
    }, delay);
    //Gets the server, then binds & starts it
    var updates = getServer();
    updates.bind('0.0.0.0:50034', grpc.ServerCredentials.createInsecure());
    updates.start();
}

main();
