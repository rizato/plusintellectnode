var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var updatesSchema = Schema({
    title: {type: String, required: true},
    url: {type: String, unique: true, required: true},
    date: {type: String, required: true}
});

module.exports = mongoose.model('Updates', updatesSchema);