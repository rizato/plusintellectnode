var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var gcmSchema = new Schema({
    token: {type: String, unique:true, required: true}
});

module.exports = mongoose.model('Gcm', gcmSchema);